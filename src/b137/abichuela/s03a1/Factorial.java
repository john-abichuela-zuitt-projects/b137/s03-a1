package b137.abichuela.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        System.out.println("Factorial\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Enter a number: ");
        int number = input.nextInt();

        int factorial = fact(number);
        System.out.println("Factorial of " + number + " is " + factorial);
    }
    static int fact(int n) {
        int output;
        if(n==1) {
            return 1;
        }
        //output = fact(n-1)*n;
        output = (n * fact(n-1));
        return output;
    }
}
